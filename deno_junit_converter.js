import { parse } from "https://deno.land/std@0.119.0/flags/mod.ts";

const flags = parse(Deno.args, {
  boolean: ["help"],
  string: ["file", "data", "output"],
  default: { help: false },
});

/**
 * Check flags
 * @param {object} flags
 * @property {boolean} help show help if true
 * @property {string} file data file
 * @property {string} data raw data
 * @property {string} output output file path
 */
export async function checkFlags(flags) {
  if (flags.help) {
    DenoUnitConverter.showHelp();
    return;
  }

  if (flags.file) {
    await DenoUnitConverter.workOnFile(flags.file, flags.output);
    return;
  }

  if (flags.data) {
    await DenoUnitConverter.workOnData(flags.data, flags.output);
    return;
  }

  console.error(
    "%cEither file or data should be provided. See following help.",
    "color: red",
  );
  DenoUnitConverter.showHelp();
  Deno.exit(1);
}

/**
 * Read a file and transform Deno test output to Junit XML
 * @param {path} file file path
 */
export async function workOnFile(file, output) {
  const text = await Deno.readTextFile(file);
  await DenoUnitConverter.workOnData(text, output);
}

/**
 * Transform Deno test output to Junit XML
 * @param {string} text Deno test output
 */
export async function workOnData(text, output) {
  const testSuites = DenoUnitConverter.getTestSuite(getTestSuites(text));
  const errorsText = DenoUnitConverter.getErrors(text);
  DenoUnitConverter.setTestSuites(testSuites, errorsText);
  const xml = DenoUnitConverter.jsonToXML(testSuites);
  if (output) {
    await Deno.writeTextFile(output, xml);
  } else {
    console.log(xml);
  }
}

/**
 * Transform Json Deno data to Junit XML
 * @param {object} json deno data as json
 * @returns a junit xml
 */
export function jsonToXML(json) {
  let xml = `<?xml version="1.0" encoding="UTF-8" ?>\n<testsuites>`;
  let i = 0;
  Object.keys(json).forEach((key) => {
    xml += `\n\t<testsuite name="${key}" id="${i}" tests="${
      json[key].nbTests
    }" failures="${json[key].nbFailed}" errors="0" skipped="0" time="0">`;
    xml += getXMLTestCases(json[key].testCases);
    xml += `\n\t</testsuite>`;
    i++;
  });
  xml += `\n</testsuites>`;
  return xml;
}

/**
 * Get test cases as XML
 * @param {object} json test cases as json
 * @returns test cases as xml
 */
export function getXMLTestCases(json) {
  let xml = "";
  Object.keys(json).forEach((key) => {
    xml += `\n\t\t<testcase name="${key}" time="0">`;
    if (json[key].message) {
      xml += `\n\t\t\t<failure type="failure" message="${
        json[key].message
      }">\n`;
      xml += json[key].content;
      xml += `\n\t\t\t</failure>`;
    }
    xml += `\n\t\t</testcase>`;
  });
  return xml;
}

/**
 * Add errors to json test suites
 * @param {object} testSuites
 * @param {string} errorsText test suites errors
 */
export function setTestSuites(testSuites, errorsText) {
  if (errorsText) {
    const errors = [];
    DenoUnitConverter.getFailuresBySuite(errorsText).forEach((failure) => {
      let myFailure = DenoUnitConverter.getOneFailureInfo(failure[0]);
      myFailure = myFailure[0];
      const error = {
        suite: myFailure[2],
        name: myFailure[1].trim(),
        message: myFailure[4],
        content: myFailure[3],
      };
      errors.push(error);
    });
    for (const error of errors) {
      testSuites[error.suite].testCases[error.name] = {
        message: error.message,
        content: error.content,
      };
    }
  }
}

/**
 * Show this CLI help
 */
export function showHelp() {
  console.log("Parse Deno test output to Junit XML.");
  console.log(
    "\n",
    "USAGE:",
    "\n",
    "\t",
    "deno run -A deno_junit.js [--help] [--file FILE] [--data DATA] [--output OUTPUT]",
  );
  console.log(
    "\n",
    "OPTIONS:",
  );
  console.log(
    "\n\t",
    "--help",
    "\n\t\t",
    "Print help information",
  );
  console.log(
    "\n\t",
    "--file",
    "\n\t\t",
    "Deno test standard output saved file",
  );
  console.log(
    "\n\t",
    "--data",
    "\n\t\t",
    "Deno test standard output",
  );
  console.log(
    "\n\t",
    "--output",
    "\n\t\t",
    "File in which to write the Junit XML, if empty result will be print",
  );
  console.log(
    "\n\n",
    "EXAMPLES",
    "\n\t",
    'echo "hey" | xargs deno run -A deno_junit.js --data',
    "\n\n\t",
    'deno run -A deno_junit.js --data "hey"',
    "\n\n\t",
    'deno run -A deno_junit.js --file "./relative/path/to/file"',
    "\n\n\t",
    'deno run -A deno_junit.js --file "/absolute/path/to/file"',
    "\n\n\t",
    'deno run -A deno_junit.js --data "hey" --output my_file.xml',
  );
}

/**
 * Get test suite
 * @param {string} text text to analyses
 * @returns a list of test suite
 */
export function getTestSuites(text) {
  const regex =
    /(?=\n?running \d+ tests? from).+?(?=\n(?:running| ERRORS|ok))/gs;
  return [...text.matchAll(regex)];
}

/**
 * Get test suite infromation
 * @param {string} text
 * @returns test suite information
 */
export function getTestSuiteInfo(text) {
  const regex = /running (\d+) tests* from (.*)/;
  return text.match(regex);
}

/**
 * Get errors/failures test details from text
 * @param {string} text
 * @returns errors part from the text
 */
export function getErrors(text) {
  const regex = /(?= ERRORS).+?(?<=FAILURES)/gs;
  const errors = text.match(regex);
  return errors != null ? errors[0] : null;
}

/**
 * Get one failure information
 * @param {string} text
 * @returns failureInfo
 */
export function getOneFailureInfo(text) {
  const regex = /((?:\w+ )+)=> (.+?):\d+:\d+.+?(error: (.+?)\n.+)/gs;
  return [...text.matchAll(regex)];
}

/**
 * Get all test suite failures
 * @param {string} text
 * @returns test suite failures
 */
export function getFailuresBySuite(text) {
  const regex =
    /(?=\n(?:\w+ )+\=\> .*:\d+:\d+\n).+?(?=(?:FAILURES|\n(?:\w+ )+\=\> .*:\d+:\d+\n))/gs;
  return [...text.matchAll(regex)];
}

/**
 * Get test suite info
 * @param {object[]} array raw test suites info
 * @returns clean test suite info
 */
export function getTestSuite(array) {
  const testSuites = {};
  array.forEach((element) => {
    const testCases = DenoUnitConverter.getTestCases(element[0]);
    const testSuite = DenoUnitConverter.getTestSuiteInfo(element[0]).slice(1);
    testSuites[testSuite[1]] = {
      nbTests: testSuite[0],
      nbFailed: 0,
      testCases: {},
    };
    // retrieve test cases info
    testCases.forEach((thisCase) => {
      testSuites[testSuite[1]].testCases[thisCase[1]] = {};
      // update suite failure info
      if (thisCase[2] == "FAILED") {
        testSuites[testSuite[1]].nbFailed++;
      }
    });
  });
  return testSuites;
}

/**
 * Get all test cases from text
 * @param {string} text
 * @returns all test cases from text
 */
export function getTestCases(text) {
  const regex = /(?:(.+) ... (ok|FAILED) \(\w+\))+/gm;
  return [...text.matchAll(regex)];
}

export const DenoUnitConverter = {
  checkFlags,
  showHelp,
  getErrors,
  getFailuresBySuite,
  getOneFailureInfo,
  getTestCases,
  getTestSuite,
  getTestSuiteInfo,
  getTestSuites,
  getXMLTestCases,
  jsonToXML,
  setTestSuites,
  workOnData,
  workOnFile,
};

if (import.meta.main) await checkFlags(flags);
