const gap = 80;
const coverageReport = await Deno.readTextFile("./coverage_report.txt");
const regexp = /(\d+\.\d+)%/g;
const array = [...coverageReport.matchAll(regexp)];

let sumOfCoverage = 0;
array.forEach((element) => {
  sumOfCoverage += +element[1];
});

const totalCoverage = (sumOfCoverage / array.length).toFixed(2);

if (totalCoverage < gap) {
  console.error(
    `Required test coverage of ${gap}% not reached. Total coverage: ${totalCoverage}%`,
  );
  Deno.exit(1);
} else {
  console.log(`Total coverage: ${totalCoverage}%`);
}
