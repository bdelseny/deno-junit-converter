```ts
Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:17:0

async function checkFlags(flags)
  Check flags

  @param {object} flags
  @property {boolean} help
      show help if true

  @property {string} file
      data file

  @property {string} data
      raw data

  @property {string} output
      output file path


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:217:0

function getErrors(text)
  Get errors/failures test details from text

  @param {string} text
  @return
      errors part from the text


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:238:0

function getFailuresBySuite(text)
  Get all test suite failures

  @param {string} text
  @return
      test suite failures


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:228:0

function getOneFailureInfo(text)
  Get one failure information

  @param {string} text
  @return
      failureInfo


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:276:0

function getTestCases(text)
  Get all test cases from text

  @param {string} text
  @return
      all test cases from text


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:249:0

function getTestSuite(array)
  Get test suite info

  @param {object[]} array
      raw test suites info

  @return
      clean test suite info


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:207:0

function getTestSuiteInfo(text)
  Get test suite infromation

  @param {string} text
  @return
      test suite information


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:196:0

function getTestSuites(text)
  Get test suite

  @param {string} text
      text to analyses

  @return
      a list of test suite


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:91:0

function getXMLTestCases(json)
  Get test cases as XML

  @param {object} json
      test cases as json

  @return
      test cases as xml


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:71:0

function jsonToXML(json)
  Transform Json Deno data to Junit XML

  @param {object} json
      deno data as json

  @return
      a junit xml


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:112:0

function setTestSuites(testSuites, errorsText)
  Add errors to json test suites

  @param {object} testSuites
  @param {string} errorsText
      test suites errors


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:138:0

function showHelp()
  Show this CLI help

Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:54:0

async function workOnData(text, output)
  Transform Deno test output to Junit XML

  @param {string} text
      Deno test output


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:45:0

async function workOnFile(file, output)
  Read a file and transform Deno test output to Junit XML

  @param {path} file
      file path


Defined in file:///workspace/deno-junit-converter/deno_junit_converter.js:281:0

const DenoUnitConverter
```
