import { DenoUnitConverter } from "../deno_junit_converter.js";
import { assertEquals } from "https://deno.land/std@0.152.0/testing/asserts.ts";
import {
  assertSpyCall,
  assertSpyCalls,
  spy,
} from "https://deno.land/std@0.152.0/testing/mock.ts";

Deno.test("Error when no flags", async () => {
  const showHelpSpy = spy(DenoUnitConverter, "showHelp");
  const workOnFileSpy = spy(DenoUnitConverter, "workOnFile");
  const workOnDataSpy = spy(DenoUnitConverter, "workOnData");
  try {
    await DenoUnitConverter.checkFlags({});
  } catch (error) {
    assertSpyCalls(showHelpSpy, 1);
    assertSpyCalls(workOnFileSpy, 0);
    assertSpyCalls(workOnDataSpy, 0);
    assertEquals(error.name, "AssertionError");
  } finally {
    showHelpSpy.restore();
    workOnFileSpy.restore();
    workOnDataSpy.restore();
  }
});

Deno.test("Show help", async () => {
  const showHelpSpy = spy(DenoUnitConverter, "showHelp");
  const workOnFileSpy = spy(DenoUnitConverter, "workOnFile");
  const workOnDataSpy = spy(DenoUnitConverter, "workOnData");
  await DenoUnitConverter.checkFlags({ help: true });
  assertSpyCalls(showHelpSpy, 1);
  assertSpyCalls(workOnFileSpy, 0);
  assertSpyCalls(workOnDataSpy, 0);
  showHelpSpy.restore();
  workOnFileSpy.restore();
  workOnDataSpy.restore();
});

Deno.test("Work on data flag", async () => {
  const consoleSpy = spy(console, "log");
  const showHelpSpy = spy(DenoUnitConverter, "showHelp");
  const workOnFileSpy = spy(DenoUnitConverter, "workOnFile");
  const workOnDataSpy = spy(DenoUnitConverter, "workOnData");
  await DenoUnitConverter.checkFlags({
    data: `
  Check file:///app/tests/returnBool_test.ts
You are running a development build of Vue.
Make sure to use the production build (*.prod.js) when deploying for production.
running 1 test from ./tests/returnBool_test.ts
toto test ... ok (8ms)

ok | 1 passed | 0 failed (62ms)
`,
  });
  assertSpyCalls(showHelpSpy, 0);
  assertSpyCalls(workOnFileSpy, 0);
  assertSpyCalls(workOnDataSpy, 1);
  assertSpyCall(consoleSpy, 0, {
    args: [await Deno.readTextFile("samples/output/junit_ok.xml")],
  });
  showHelpSpy.restore();
  workOnFileSpy.restore();
  workOnDataSpy.restore();
  consoleSpy.restore();
});

Deno.test("Work on file flag", async () => {
  const consoleSpy = spy(console, "log");
  const showHelpSpy = spy(DenoUnitConverter, "showHelp");
  const workOnFileSpy = spy(DenoUnitConverter, "workOnFile");
  const workOnDataSpy = spy(DenoUnitConverter, "workOnData");
  await DenoUnitConverter.checkFlags({ file: "samples/deno_tests_ok.log" });
  assertSpyCalls(showHelpSpy, 0);
  assertSpyCalls(workOnFileSpy, 1);
  assertSpyCalls(workOnDataSpy, 1);
  assertSpyCall(consoleSpy, 0, {
    args: [await Deno.readTextFile("samples/output/junit_ok.xml")],
  });
  showHelpSpy.restore();
  workOnFileSpy.restore();
  workOnDataSpy.restore();
  consoleSpy.restore();
});

Deno.test("Check output", async () => {
  const fileName = "junit_output.xml";
  const writeSpy = spy(Deno, "writeTextFile");
  const expected = await Deno.readTextFile("samples/output/junit_ok.xml");
  await DenoUnitConverter.checkFlags({
    file: "samples/deno_tests_ok.log",
    output: fileName,
  });
  assertSpyCall(writeSpy, 0, { args: [fileName, expected] });
  writeSpy.restore();
  Deno.removeSync(fileName);
});

Deno.test("Get test failures", async () => {
  const consoleSpy = spy(console, "log");
  await DenoUnitConverter.checkFlags({
    file: "samples/deno_tests_failure.log",
  });
  assertSpyCall(consoleSpy, 0, {
    args: [await Deno.readTextFile("samples/output/junit_failure.xml")],
  });
  consoleSpy.restore();
});
