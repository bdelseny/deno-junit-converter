# Deno to Junit converter

Intend to transform Deno test output to a Junit XML file.

## Getting Started

Intructions on installation and use.

### Prerequisites

- Deno
  ```
  Give examples
  ```

#### To go further

- Genhtml
- lcov_cobertura
- checkCoverage

### Installing

TODO

## Mapping information

See [mapping](mapping.md)

## Built With

- [Deno](https://deno.land/) - A modern runtime for JavaScript and TypeScript

## Contributing

_TODO: Add contributing doc_

## Authors

- **Bastien Delseny** - _Initial work / Maintainer_ -
  [deno_junit_converter](https://gitlab.com/bdelseny/deno-junit-converter)

_TODO: Add contributors_

## License

This project is licensed under the GNU General Public License v3 - see the
[LICENSE](LICENSE.md) file for details

## Acknowledgments

Documentation page is generated with [docsify](https://docsify.js.org/).

## JSDOC

See [JSDoc](jsdoc.md).

Generated with:

````bash
echo '```ts' > jsdoc.md
deno doc deno_junit_converter.js >> jsdoc.md
echo '```' >> jsdoc.md
````

## Dependencies

See [dependencies tree](info.md).

Generated with:

````bash
echo '```bash' > info.md
deno info deno_junit_converter.js >> info.md
echo '```' >> info.md
````
